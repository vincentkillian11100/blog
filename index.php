<?php
// Récup date
$date = time();
// L'url jusqu'a data/
$myUrl = 'http://localhost/blog/data/';
// L'url entiere en fonction de la date de la publication
$url = $myUrl . $date . '.json';
// Récuperer la valeur du textArea
$content = $_POST["content"];
// Chemin du json en fonction de la date 
$data = './data/' . $date . '.json';
// Tableaux contenant les variables correspondant au date.json
$tab = array('url' => $url, 'content' => $content, 'date' => date('m-d-Y h:i:s'));
// Recuperer la valeur de l'input secret
$secret = $_POST['secret'];
// Chemin accedant au posts
$chemin = './data/posts.json';
// On lis le fichier ../pdw.json tout en le decodant du json pour l'avoir en tableaux
$secretMdp = json_decode(file_get_contents('./pdw.json'),true);
// crypt va nous servire a crypter la variable 'secret' en utilisant la clef hey de type sha1
// si la variable 'mdp' et égale au hash de secret alor
if (hash_equals($secretMdp['mdp'],crypt($_POST['secret'], sha1('hey')))) {
    // Dans 'data' on écrit les donner de tab en enlevant les slashs (tout en encodant le tab en json)
    file_put_contents($data, json_encode($tab, JSON_UNESCAPED_SLASHES));
    // On lit les posts en le mettant dans une variable
    $lire = file_get_contents('./data/posts.json');
    // on decode la var 'lire'
    $decod = json_decode($lire, true);
    // on empile les url dans le tableau d'entrer 'decod' dans la valeur post 
    array_push($decod['post'], $url);
    //On ecrit le tableau array_push (en l'encodant) dans le chemin 
    file_put_contents($chemin, json_encode($decod,JSON_UNESCAPED_SLASHES));
} else {
    // Si non on affiche une erreur 403 
     header("HTTP/1.1 403 Le secret n'est pas bon");
}

include 'vue.html';
